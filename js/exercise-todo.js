//On crée une instance de TodoList
let list = new TodoList();

//On rajoute 4 tasks dedans avec la méthode faite pour
list.addTask("test");
list.addTask("bloup");
list.addTask("blop");
list.addTask("blip");

//On sélectionne l'élément qui recevra la TodoList
let target = document.querySelector("#target");

//On append à cet élément le résultat du draw() de la TodoList
target.appendChild(list.draw());
