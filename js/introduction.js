class Dog {
    constructor(name, breed, personality, birthdate, size) {
    this.name = name;
       this.breed = breed;
       this.personality = personality;
       this.birthdate = birthdate;
       this.size = size;
    }
};
  


let dog = new Dog("Fido", "corgi", "calm", "10/03/2016", "medium");

presentation(dog);

let dog2 = createDog("Rex", "doggo", "cool", "10/03/2015", "big");

presentation(dog2);

function presentation(paramDog) {
  let p = document.createElement("p");
  p.textContent = `Henlo, my name is ${paramDog.name}, I am a ${paramDog.size} and ${paramDog.personality} ${paramDog.breed} born on ${paramDog.birthdate}. I am good dog.`;
  let target = document.querySelector("#target");
  target.appendChild(p);
}


function createDog(name, breed, personality, birthdate, size){
   let myDog = {
       name: name,
       breed: breed,
       personality: personality,
       birthdate: birthdate,
       size: size
   }
       return myDog;
};
  
