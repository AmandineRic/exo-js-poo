class Counter{
    constructor(){
        this.value = 0;
        }
        increment(){
            this.value++;
            return this.value;
        }
        decrement() {
            this.value--;
            return this.value;
        }
        reset(){
            this.value = 0;
            return this.value;
        }
} ;