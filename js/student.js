class Student {
  /**
   * @param {String} nameStudent
   * @param {String} ageStudent
   * @param {String} promoStudent
   *
   */
  constructor(nameStudent, ageStudent, promoStudent) {
    this.name = nameStudent;
    this.age = ageStudent;
    this.promotion = promoStudent;
    this.graduated = false;
  }
}
