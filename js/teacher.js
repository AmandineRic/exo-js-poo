class Teacher {
  /**
   * @param {String} nameTeacher
   * @param {String} specialityTeacher
   */
  constructor(nameTeacher, specialityTeacher) {
    this.name = nameTeacher;
    this.speciality = specialityTeacher;
    this.hired = true;
  }
}