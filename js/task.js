/**
 * Classe représentant une tâche à faire avec une description de
 * la tâche et une propriété done indiquant si la tâche est faite ou non
 */
class Task {
    /**
     * @param {string} paramLabel Description de la tâche à faire
     */
    constructor(paramLabel) {
        this.label = paramLabel;
        this.done = false;
    }
    /**
     * Méthode qui change la propriété done de true à false ou
     * inversement
     */
    toggleDone() {
        this.done = !this.done;

        // if(this.done) {
        //     this.done = false;
        // } else {
        //     this.done = true;
        // }
    }
/**
 * @returns{Element}
 */

    draw(){
        let li = document.createElement("li");
        li.textContent = this.label;
        let checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        li.appendChild(checkbox);

        if(this.done){
            checkbox.checked = true;
        }
        checkbox.addEventListener('input', () => this.toggleDone());

        return li;
    }
}








// class Task {
//     /**
//      * 
//      * @param {string} label 
//      */


//     constructor(label){
//         this.label = label;
//         this.done = false;
//     }
//     toggleDone(){
//      this.done = !this.done;
// }}