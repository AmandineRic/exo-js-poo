class TodoList {
  constructor() {
    /**
     * @type Task[]
     */
    this.tasks = [];
  }
  /**
   * Une méthode permettant de rajouter une Task à la liste des todo
   * @param {string} description la tache à effectuer
   */
  addTask(description) {
    let task = new Task(description);
    this.tasks.push(task);
    //        this.tasks.push(new Task(description));
  }
  /**
   * Méthode qui supprimes toutes les tâches qui sont terminées dans la liste
   */
  clearDone() {
    //version filter + fat arrow
    this.tasks = this.tasks.filter(item => !item.done);
    //version filter + fonction anonyme
    // this.tasks = this.tasks.filter(function(item) {
    //     return !item.done;
    // });

    //version avec une boucle classique
    // for (let index = this.tasks.length - 1; index >= 0; index--) {
    //     const task = this.tasks[index];
    //     if(task.done) {
    //         this.tasks.splice(index, 1);
    //     }

    // }
  }
  draw(){
    let ul = document.createElement("ul");

    for (const item of this.tasks) {
      ul.appendChild(item.draw());
    }
    return ul;
  }
  target(){
    
  }
}





// class TodoList {
    
//     constructor(){
//         /**
//          * @type Task[]
//          */
//         this.tasks = [];
//     }
//     /** the method add a task to my to do list
//      * @param {string} description
//      */
//     addTask(description){
//         let task = new Task(description);
//         this.tasks.push(task);
//     }
//     clearDone(){
//         this.tasks = this.tasks.filter(item => !item.done);
//     }
// }